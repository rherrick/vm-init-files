#!/usr/bin/env bash
# Sets up shell init files for root and xnat users. Works for CentOS and Ubuntu.

getShellPath() {
    local SHELL_NAME=${1}
    local SHELL_PATH=$(which ${SHELL_NAME})
    [[ -z ${SHELL_PATH} ]] && { echo "/usr/bin/${SHELL_NAME}"; } || { echo "${SHELL_PATH}"; }
}

checkUserExists() {
    local USERNAME=${1}
    id ${USERNAME} &> /dev/null
    echo ${?}
}

export BASH_EXEC="$(getShellPath bash)"
export ZSH_EXEC="$(getShellPath zsh)"

eval $(fgrep ID= /etc/os-release)

case ${ID} in
    centos)
        INSTALL_CMD=yum
        yum --assumeyes upgrade
        ;;
    debian|ubuntu)
        INSTALL_CMD="sudo DEBIAN_FRONTEND=noninteractive apt-get"
        ${INSTALL_CMD} --assume-yes update
        ${INSTALL_CMD} --assume-yes full-upgrade
        ;;
    *)
        echo Unknown OS "${ID}", exiting
        exit -1
        ;;
esac

${INSTALL_CMD} -y install zsh zip unzip git curl

git clone --quiet https://github.com/zsh-users/antigen.git /usr/local/share/antigen
git clone --quiet https://github.com/zsh-users/zsh-autosuggestions.git /usr/local/share/zsh-autosuggestions
sed --in-place 's/auth[[:space:]]\+required[[:space:]]\+/auth\t\tsufficient\t/' /etc/pam.d/chsh

# Execute first to install iterm2 tools for bash
echo "Installing iTerm2 tools for bash for user root..."
sudo --login DEBIAN_FRONTEND=noninteractive <<AS_ROOT
    http --ignore-stdin --body https://iterm2.com/shell_integration/install_shell_integration_and_utilities.sh | ${BASH_EXEC}
AS_ROOT

# Change login shell to zsh
echo "Setting root shell to ${ZSH_EXEC}..."
chsh --shell ${ZSH_EXEC}
unzip -o package.zip -d /root

# Execute again to install iterm2 tools for zsh
echo "Installing iTerm2 tools for zsh for user root..."
sudo --login DEBIAN_FRONTEND=noninteractive <<AS_ROOT
    http --ignore-stdin --body https://iterm2.com/shell_integration/install_shell_integration_and_utilities.sh | ${BASH_EXEC}
AS_ROOT

[[ -f /root/update-zsh-dependencies ]] && {
    echo "Moving /root/update-zsh-dependencies to /etc/cron.daily";
    mv /root/update-zsh-dependencies /etc/cron.daily
    chmod +x /etc/cron.daily/update-zsh-dependencies
}

VAGRANT_EXISTS=$(checkUserExists vagrant)
[[ ${VAGRANT_EXISTS} == 0 ]] && {
    echo "User vagrant exists, setting up home folder"
    find ~vagrant -user root -exec chown vagrant.vagrant '{}' \;
}

XNAT_EXISTS=$(checkUserExists xnat)
[[ ${XNAT_EXISTS} == 0 ]] && {
    echo "User xnat exists, setting up groups"
    [[ ${VAGRANT_EXISTS} == 0 && $(groups xnat | fgrep vagrant | wc -l) == 0 ]] && { usermod --groups=vagrant --append xnat; }
} || {
    echo "User xnat does not exist, creating and setting up groups"
    mkdir -p /data/xnat
    useradd --shell=${BASH_EXEC} --groups=vagrant --home-dir=/data/xnat/home --create-home xnat
}

echo "xnat ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/98_xnat
unzip -o package.zip -d ~xnat
[[ -f ~xnat/update-zsh-dependencies ]] && { rm -f ~xnat/update-zsh-dependencies; }
mkdir -p ~xnat/.tmux/plugins
git clone --quiet https://github.com/tmux-plugins/tpm.git ~xnat/.tmux/plugins/tpm
find /data -user root -exec chown xnat.xnat '{}' \;

# Execute first to install iterm2 tools for bash
echo "Installing iTerm2 tools for bash for user xnat..."
su --login xnat <<AS_XNAT
    http --ignore-stdin --body https://iterm2.com/shell_integration/install_shell_integration_and_utilities.sh | ${BASH_EXEC}
AS_XNAT

# Set shell to zsh
echo "Setting xnat shell to ${ZSH_EXEC}, installing tmux plugins..."
chsh --shell ${ZSH_EXEC} xnat

# Install iTerm2 tools for zsh, SDKMan
su --login xnat <<AS_XNAT
    echo "Installing iTerm2 tools for zsh for user xnat..."
    http --ignore-stdin --body https://iterm2.com/shell_integration/install_shell_integration_and_utilities.sh | ${BASH_EXEC}
    echo "Installing SDKMan tools for user xnat..."
    http --ignore-stdin --body https://get.sdkman.io | ${BASH_EXEC}
AS_XNAT

[[ -f /root/.ssh/authorized_keys ]] && { rm -f /root/.ssh/authorized_keys; }
for DIR in /root/bin /root/.ssh ~xnat/bin ~xnat/.ssh; do
    [[ -d ${DIR} ]] && { (($(find ${DIR} -mindepth 1 | wc -l) == 0)) && { rmdir ${DIR}; }; };
done

echo "VM Init Files installation completed"