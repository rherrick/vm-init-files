#!/bin/bash

for TARGET in package.zip vm-init-files.zip vm-init-files.zip.sha256; do
    [[ -f ${TARGET} ]] && { rm -f ${TARGET}; }
done

cd package
zip ../package.zip $(find . -type f)
cd ..

zip vm-init-files.zip setup.sh package.zip
shasum -a 256 vm-init-files.zip > vm-init-files.zip.sha256
rm -f package.zip

