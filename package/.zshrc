# zmodload zsh/zprof

PLATFORM=$(uname)
[[ "${PLATFORM}" == "Darwin" ]] && { ANTIGEN_ROOT=$(brew --prefix)/share/antigen; } || { [[ -d ${HOME}/.zsh/antigen ]] && { ANTIGEN_ROOT=${HOME}/.zsh/antigen; } || { ANTIGEN_ROOT=/usr/local/share/antigen; }; }

source ${ANTIGEN_ROOT}/antigen.zsh
source ${HOME}/.zsh/vars
source ${HOME}/.zsh/aliases

antigen use oh-my-zsh

[[ "${PLATFORM}" == "Darwin" ]] && {
    zstyle :omz:plugins:ssh-agent agent-forwarding on
    zstyle :omz:plugins:ssh-agent identities id_ed25519 id_rsa

    antigen bundles << BUNDLES
        aws
        brew
        kube-ps1
        kubectl
        lukechilds/zsh-nvm
        osx
        ssh-agent
        thefuck
        vagrant
BUNDLES
}

antigen bundles << BUNDLES
    command-not-found
    common-aliases
    docker
    docker-compose
    git
    gradle
    history
    httpie
    iterm2
    jsontools
    mvn
    node
    pip
    postgres
    python
    spring
    tmux
    yarn
    zsh-navigation-tools
    zsh-users/zsh-autosuggestions
    zsh-users/zsh-completions
    zsh-users/zsh-history-substring-search
    zsh-users/zsh-syntax-highlighting
BUNDLES

# Fix lag when pasting to terminal caused by zsh syntax highlighting
zstyle ':bracketed-paste-magic' active-widgets '.self-*'

antigen theme candy

antigen apply

ZSH_AUTOSUGGEST_ROOT=$(dirname ${ANTIGEN_ROOT})/zsh-autosuggestions
[[ -f ${ZSH_AUTOSUGGEST_ROOT}/zsh-autosuggestions.zsh ]] && { source ${ZSH_AUTOSUGGEST_ROOT}/zsh-autosuggestions.zsh; zle -N zle autosuggest-start; }

for CMD in compinit bashcompinit; do
    autoload -U +X ${CMD}
    ${CMD}
done

for CMD in helm kubectl; do
    [[ $(command -v ${CMD}) == 0 ]] && { source <(${CMD} completion zsh); }
done

export PROMPT='%{$fg_bold[green]%}%n@%m %{$fg[blue]%}%D{[%X]} %{$reset_color%}%{$fg[white]%}[%~]%{$reset_color%} $(git_prompt_info)'$'\n''%{$fg[blue]%}-> %(?.%{$fg_bold[green]%}.%{$fg_bold[red]%})$[HISTCMD]%{$fg[blue]%}[%(?.%{$fg_bold[green]%}√.%{$fg_bold[red]%}%?)%{$fg[blue]%}] %{$fg_bold[blue]%}#%{$reset_color%} '

[[ ! -z "${TMUX}" ]] && { export SDKMAN_INIT=false; }
for INIT_SCRIPT in "${HOME}/.iterm2_shell_integration.zsh" "${HOME}/.sdkman/bin/sdkman-init.sh" "${HOME}/.fzf.zsh" "${HOME}/.cargo/env" "${NVM_DIR}/bash_completion"; do
    [[ -s ${INIT_SCRIPT} ]] && { source ${INIT_SCRIPT}; }
done

# zprof
